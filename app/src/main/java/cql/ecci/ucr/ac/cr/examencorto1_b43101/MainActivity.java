package cql.ecci.ucr.ac.cr.examencorto1_b43101;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    // UI items list
    private ListView listView;

    // ArrayLists with for the database info
    private ArrayList<String> tableTopNames;
    private ArrayList<String> tableTopDescriptions;
    private ArrayList<Integer> tableTopIcons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //DB Inserts
        insertTableTop1();
        insertTableTop2();
        insertTableTop3();
        insertTableTop4();
        insertTableTop5();

        listView = (ListView) findViewById(R.id.list);
        tableTopNames = new ArrayList<>();
        tableTopIcons = new ArrayList<>();
        tableTopDescriptions = new ArrayList<>();
        // Read and add each table top to the lists
        readTableTop("TT001");
        readTableTop("TT002");
        readTableTop("TT003");
        readTableTop("TT004");
        readTableTop("TT005");

        CustomListAdapter adapter = new CustomListAdapter(this, tableTopNames, tableTopIcons, tableTopDescriptions);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent itemInfo = new Intent(MainActivity.this, TableTopInfoActivity.class);
                itemInfo.putExtra("Position", position);
                startActivity(itemInfo);
            }
        });


    }

    // Insert Method for the first Table Top
    private void insertTableTop1() {
        TableTop tableTop = new TableTop(
                "TT001",
                "Catan",
                "1995",
                "Kosmos",
                "Germany",
                48.774538,
                9.188467,
                "Picture yourself in the era of discoveries:after a long voyage of great deprivation,your ships have finally reached the coast ofan uncharted island. Its name shall be Catan!But you are not the only discoverer. Otherfearless seafarers have also landed on theshores of Catan: the race to settle theisland has begun!",
                "3-4",
                "10+",
                "1-2 hours"
        );
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Insert Method for the second Table Top
    private void insertTableTop2() {
        TableTop tableTop = new TableTop(
                "TT002",
                "Monopoly",
                "1935",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel!",
                "2-8",
                "8+",
                "20-180 minutes"
        );
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Insert Method for the third Table Top
    private void insertTableTop3() {
        TableTop tableTop = new TableTop(
                "TT003",
                "Eldritch Horror",
                "2013",
                "Fantasy Flight Games",
                "United States",
                45.015417,
                -93.183995,
                "An ancient evil is stirring. You are part ofa team of unlikely heroes engaged in an international struggle to stop the gathering darkness. To do so, you’ll have to defeat foul monsters, travel to Other Worlds, andsolve obscure mysteries surrounding thisunspeakable horror.",
                "1-8",
                "14+",
                "2-4 hours"
        );
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Insert Method for the fourth Table Top
    private void insertTableTop4() {
        TableTop tableTop = new TableTop(
                "TT004",
                "Magic: the Gathering",
                "1993",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "Magic: The Gathering is a collectible and digital collectible card game created byRichard Garfield. Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts,and summon creatures.",
                "2+",
                "13+",
                "Varies"
        );
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Insert Method for the last Table Top
    private void insertTableTop5() {
        TableTop tableTop = new TableTop(
                "TT005",
                "Hanabi",
                "2010",
                "Asmodee",
                "France",
                48.761629,
                2.065296,
                "Hanabi—named for the Japanese word for\"fireworks\"—is a cooperative game in whichplayers try to create the perfect fireworks show by placing the cards on the table in theright order.",
                "2-5",
                "8+",
                "25 minutes"
        );
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Method that reads and assigns the info from the DB
    public void readTableTop(String id) {
        TableTop tableTop = new TableTop();
        tableTop.read(getApplicationContext(), id);
        tableTopDescriptions.add(tableTop.getDescription());
        tableTopNames.add(tableTop.getName());
        switch (id) {
            case "TT001":
                tableTopIcons.add(R.drawable.catan);
                break;
            case "TT002":
                tableTopIcons.add(R.drawable.monopoly);
                break;
            case "TT003":
                tableTopIcons.add(R.drawable.eldritch);
                break;
            case "TT004":
                tableTopIcons.add(R.drawable.mtg);
                break;
            case "TT005":
                tableTopIcons.add(R.drawable.hanabi);
                break;
        }
    }
}
