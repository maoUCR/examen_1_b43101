package cql.ecci.ucr.ac.cr.examencorto1_b43101;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

// Activity for the display of each TableTop individually
public class TableTopInfoActivity extends AppCompatActivity {
    String name;
    String description;
    String filename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_activity);

        Intent infoIntent = getIntent( );
        int position = infoIntent.getIntExtra("Position",0);
        switch (position){
            case 0:
                readTableTop("TT001");
                break;
            case 1:
                readTableTop("TT002");
                break;
            case 2:
                readTableTop("TT003");
                break;
            case 3:
                readTableTop("TT004");
                break;
            case 4:
                readTableTop("TT005");
                break;
        }
    }

    // Read data from DB
    private void readTableTop(String id) {
        TableTop tableTop = new TableTop();
        tableTop.read(getApplicationContext(), id);

        name = tableTop.getName();
        description = tableTop.getDescription();

        switch (id) {
            case "TT001":
                filename = "catan";
                break;
            case "TT002":
                filename = "monopoly";
                break;
            case "TT003":
                filename = "eldritch";
                break;
            case "TT004":
                filename = "mtg";
                break;
            case "TT005":
                filename = "hanabi";
                break;
        }

        TextView nameView = (TextView) findViewById(R.id.name);
        TextView descView = (TextView) findViewById(R.id.description);
        ImageView iconView = (ImageView) findViewById(R.id.icon);

        int resId = getResources().getIdentifier(filename, "drawable", getPackageName());

        nameView.setText(name);
        descView.setText(description);
        iconView.setImageResource(resId);
    }
}
