package cql.ecci.ucr.ac.cr.examencorto1_b43101;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

// Adapter programmed during laboratories, used here for the array of TableTops
public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> itemName;
    private final ArrayList<Integer> imgId;
    private final ArrayList<String> itemDescription;
    public CustomListAdapter(Activity context, ArrayList<String> itemname, ArrayList<Integer> imgid, ArrayList<String>
            itemdescription) {
        super(context, R.layout.custom_list, itemname);
        this.context = context;
        this.itemName = itemname;
        this.imgId = imgid;
        this.itemDescription = itemdescription;
    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list, null, true);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        ImageView image = (ImageView) rowView.findViewById(R.id.icon);
        TextView description = (TextView) rowView.findViewById(R.id.description);
        name.setText(itemName.get(position));
        image.setImageResource(imgId.get(position));
        description.setText(itemDescription.get(position));
        return rowView;
    }
}