package cql.ecci.ucr.ac.cr.examencorto1_b43101;

import android.provider.BaseColumns;

public final class DataBaseContract {

    // Private constructor to avoid instancing of the class
    private DataBaseContract() {}

    // Internal class to define all tables and columns
    // Implements BaseColumns to inherit standard fields from Android
    public static class DataBaseEntry implements BaseColumns {

        // TABLE
        public static final String TABLE_NAME_TABLETOP = "tableTop";

        // COLUMNS

        //private String id
        public static final String COLUMN_NAME_ID = "id";
        //private String name
        public static final String COLUMN_NAME_NAME = "name";
        //private String year
        public static final String COLUMN_NAME_YEAR = "year";
        //String publisher
        public static final String COLUMN_NAME_PUBLISHER = "publisher";
        //private String country
        public static final String COLUMN_NAME_COUNTRY = "country";
        //private double latitude
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        //private double longitude
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        //private String description
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        //private String numPlayers
        public static final String COLUMN_NAME_NUMPLAYERS = "numPlayers";
        //private String ages
        public static final String COLUMN_NAME_AGES = "ages";
        //private String playingTime
        public static final String COLUMN_NAME_PLAYINGTIME = "playingTime";
    }

    // DB TYPES
    private static final String TEXT_TYPE = " TEXT";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    // TABLE CREATE
    public static final String SQL_CREATE_TABLETOP =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + " (" +
                    DataBaseEntry.COLUMN_NAME_ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + DOUBLE_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + DOUBLE_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NUMPLAYERS + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PLAYINGTIME + TEXT_TYPE + " )";

    // TABLE DELETE
    public static final String SQL_DELETE_TABLETOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;
}
